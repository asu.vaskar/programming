package com.inn.NumberProgramming;

import java.util.Scanner;

public class FibbonacciSeries {

	public static void main(String[] args) {
		System.out.println("hello ");
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int num=sc.nextInt();
		fibonacciSeries(num);

	}
	static void fibonacciSeries(int num) {
		int num1=0;
		int num2=1;
		if(num ==0 || num ==1)
			System.out.println("fibonacci series are "+num);
		else {
			int count =0;
			while(count<num) {
				System.out.print(num1+" ");
				int num3=num1+num2;
				num1=num2;
				num2=num3;
				count++;
			}
		}
	}

}
/*SHA256:dXJDf1LqO3xmrD+VYLjTEzhOJK9SDfxkrLMr03Y8KE8 <ashis.chawley123@gamil.com>
eval $(ssh-agent -s)
cat ~/.ssh/id_ed25519.pub | clip

ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJFtXRL0oO1QQUVb7o5xaXSAV54sjXQ/4J05jwEAUk/l <ashis.chawley123@gamil.com>
 ssh-keygen -t ed25519 -C "<ashis.chawley123@gamil.com>"
 ssh-add ~/.ssh/id_ed25519


git push -uf origin main
git remote add origin https://gitlab.com/asu.vaskar/programming.git
 git pull --rebase origin main*/